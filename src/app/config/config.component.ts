import { Component, OnInit } from '@angular/core';
import {FormBuilder, NgForm} from '@angular/forms';
import {ApiService} from '../services/api.service';
import {from, Observable} from 'rxjs';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent implements OnInit {

  settingsControl: any;
  siteDomain =  localStorage.getItem('siteDomain');
  siteToken =  localStorage.getItem('siteToken');
  constructor(private apiService: ApiService) {

  }

  ngOnInit() {

    if (this.siteDomain && this.siteToken){
      this.settingsControl = {
        siteDomain: this.siteDomain,
        siteToken: this.siteToken
      }
    }else {
      this.resetForm();
    }

  }

  resetForm(){
    this.settingsControl = {
      siteDomain: '',
      siteToken: ''
    }

  }

  savePanel(form: NgForm) {

    console.warn('Your order has been submitted', form);

    localStorage.setItem('siteDomain', form.value['siteDomain']);
    localStorage.setItem('siteToken', form.value['siteToken']);
    location.reload();

  }

  testConnection(){
    this.apiService.readyStatus().subscribe(res => {
      alert("Is ready: "+res['is_ready']);
      console.log(res);
    });
  }



}
