import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {ConfigComponent} from './config/config.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HighchartsChartModule} from 'highcharts-angular';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatListModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatSliderModule, MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {NavComponent} from './nav/nav.component';
import {LayoutModule} from '@angular/cdk/layout';
import {ChartComponent} from './highcharts/chart/chart.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgMatSearchBarModule} from 'ng-mat-search-bar';
import { GraphComponent } from './components/graph/graph.component';
import { RecordsComponent } from './components/records/records.component';
import { ModelComponent } from './components/model/model.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ConfigComponent,
    NavComponent,
    ChartComponent,
    GraphComponent,
    RecordsComponent,
    ModelComponent
  ],
  imports: [
    HighchartsChartModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatListModule,
    LayoutModule,
    MatSliderModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    NgMatSearchBarModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
