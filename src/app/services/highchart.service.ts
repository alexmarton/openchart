import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HighchartService {

  rates$: Observable<chartModal[]>;

  constructor() {
  }
}

export interface chartModal {
  currency: string,
  rate: number
}
