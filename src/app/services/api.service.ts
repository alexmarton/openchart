import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {CollectionService} from './abstract/collection.service';


@Injectable({
  providedIn: 'root'
})
export class ApiService extends CollectionService {

  params = new HttpParams();


  constructor(private httpClient: HttpClient) {
    super();
    this.params = this.params.append('auth', this.token);
  }

  public readyStatus(){
    this.params = this.params.append('request', 'ready_status');
    return this.httpClient.get(this.api, {params:this.params});
  }

  public aliveStatus(){
    this.params = this.params.append('request', 'alive_status');
    return this.httpClient.get(this.api, {params:this.params} );
  }


  public getGraph(indexName: any){
    this.params = this.params.append('request', 'indices/'+indexName);
    return this.httpClient.get(this.api, {params:this.params} );
  }

  public getListGraph(){
    this.params = this.params.append('request', 'indices');
    return this.httpClient.get(this.api, {params:this.params} );
  }

  public getModel(){
    this.params = this.params.append('request', 'model');
    return this.httpClient.get(this.api, {params:this.params} );
  }

  public getListRecords(){
    this.params = this.params.append('request', 'records');
    return this.httpClient.get(this.api, {params:this.params} );
  }

  public getSingleRecords(id: any){
    this.params = this.params.append('request', 'records/'+id);
    return this.httpClient.get(this.api, {params:this.params} );
  }

  public getSingleRecordsRelations(id: any){
    this.params = this.params.append('request', 'records/'+id+'/relationships')
    return this.httpClient.get(this.api, {params:this.params} );
  }

  public getSingleRecordsRelation(id: any, relid:any){
    this.params = this.params.append('request', 'records/'+id+'/relationships/'+relid);
    return this.httpClient.get(this.api, {params:this.params} );
  }

  public postSearch(){
    this.params = this.params.append('request', 'search');
    return this.httpClient.get(this.api, {params:this.params} );
  }

  public getTraversalTemplates(){
    this.params = this.params.append('request', 'traversal_template');
    return this.httpClient.get(this.api, {params:this.params} );
  }

  public get() {
    let reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.httpClient.get(this.api,{headers: reqHeader} );
  }

  public getSource() {
    let reqHeader = new HttpHeaders({'Content-Type': 'application/json', 'No-Auth': 'True'});
    return this.httpClient.get(this.api,{headers: reqHeader} );
  }

}
