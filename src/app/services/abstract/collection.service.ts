import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {throwError} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  protected httpOptions: any;
  protected readonly api = localStorage.getItem('siteDomain');
  protected token = localStorage.getItem('siteToken');

  constructor() {
  }

  protected getHttpOptions(params: HttpParams = null): any {

    // his.logger.info('CollectionService: getHttpOptions()');

    if (!this.httpOptions) {


      this.httpOptions = {
        headers: new HttpHeaders({
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET,HEAD,OPTIONS,POST,PUT',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
          'Content-Type': 'application/json, text/plain, */*'
        })
      };

      console.info(JSON.stringify(this.httpOptions));
    }

    this.httpOptions.params = params;

    console.info(JSON.stringify(this.httpOptions));

    return this.httpOptions;
  }

  protected handleError(error: HttpErrorResponse) {

    if (error.error instanceof ErrorEvent) {

      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);

    } else {

      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');

  }

}
