import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api.service';
import {Results} from '../../models/Result';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {

  message:any;
  statusRequest: any = false;
  statusCode: any = 200;
  statusMessage: any = "disable";
  results: any = Results;
  displayedColumns: string[] = ['index_type', 'name', 'weight'];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getListGraph();
  }

  getListGraph(){
    this.apiService.getListGraph().subscribe(res => {
      console.log(res);
      this.results = res;

      if(this.results.status_code === 401){
        this.statusCode=401;
        this.statusRequest=true;
        this.statusMessage="warning";
        this.message = "Problem processing request. The user is not authenticated.";
      }
      if(this.results.status_code === 403){
        this.statusCode=403;
        this.statusRequest=true;
        this.statusMessage="warning";
        this.message = "Problem processing request. The user is not authorized to perform the request. (MDC-Manager require)";
      }
      if(this.results.status_code === 500){
        this.statusCode=500;
        this.statusRequest=true;
        this.statusMessage="warning";
        this.message = "Problem getting indices. An internal error occurred while attempting to retrieve indices.";
      }

      if(this.results.indices.length < 1){
        this.statusRequest=true;
        this.statusMessage="info";
        this.message = "Records are empty";
      }



    }, error => {

      console.log(error.status_code);

      if(error.status){
        this.statusRequest=true;
        this.statusMessage="error";
        this.message = "Change the token";
      }

    });
  }

}
