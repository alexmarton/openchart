import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {ConfigComponent} from './config/config.component';
import {ChartComponent} from './highcharts/chart/chart.component';
import {GraphComponent} from './components/graph/graph.component';
import {RecordsComponent} from './components/records/records.component';




export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent, },
  { path: 'charts', component: ChartComponent, },
  { path: 'config', component: ConfigComponent, },
  { path: 'about', component: AboutComponent },
  { path: 'graph', component: GraphComponent },
  { path: 'records', component: RecordsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
