etrieve Pre-Market data from Google Finance
Ported to Python 3.x
Original code by Dr. Pawel Lachowicz
http://www.quantatrisk.com/2015/05/07/hacking-google-finance-in-pre-market-trading-python/
"""

import requests
import json
from time import sleep


def fetchPreMarket(symbol, exchange):
        link    = "http://finance.google.com/finance/info?client=ig&q="
            url     = link + "%s:%s" % (exchange, symbol)
                content = requests.get(url).text
                    info    = json.loads(content[3:])[0]
                        ts      = str(info["elt"])    # time stamp
                            last    = float(info["l"])    # close price (previous trading day)
                                pre     = float(info["el"])   # stock price in pre-market (after-hours)
                                    return (ts, last, pre)

                                    if __name__ == '__main__':
                                            prev = 0
                                                while True:
                                                        ts, last, pre = fetchPreMarket("AAPL", "NASDAQ")
                                                                if (pre != prev):
                                                                            prev = pre
                                                                                        print(("%s\t%.2f\t%.2f\t%+.2f\t%+.2f%%" %
                                                                                                           (ts, last, pre, pre - last, (pre / last - 1) * 100.)))
                                                                                                                   sleep(60)
