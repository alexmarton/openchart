FROM node:11.6.0-alpine AS builder
COPY . ./openchart
WORKDIR /openchart
RUN npm i
RUN $(npm bin)/ng build --prod

FROM nginx:stable
## Remove default file
#RUN rm /etc/nginx/conf.d/default.conf
## Copy our nginx config
#COPY conf/default.conf /etc/nginx/conf.d/
## Remove default nginx website
#RUN chmod -R 777 /var/log/nginx /var/cache/nginx/ && chmod -R 777 /etc/nginx/* && chmod -R 777 /var/run/ && chmod -R 777 /usr/share/nginx/
## copy over the artifacts in dist folder to default nginx public folder
# support running as arbitrary user which belogs to the root group
RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx
# users are not allowed to listen on priviliged ports
RUN sed -i.bak 's/listen\(.*\)80;/listen 8082;/' /etc/nginx/conf.d/default.conf
EXPOSE 8082
# comment user directive as master process is run as user in OpenShift anyhow
RUN sed -i.bak 's/^user/#user/' /etc/nginx/nginx.conf
COPY --from=builder /openchart/dist/openchart/ /usr/share/nginx/html

